package com.activiti.extension.bean;

import java.util.HashMap;
import java.util.Map;

import org.activiti.engine.RuntimeService;
import org.activiti.engine.runtime.Execution;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;



@Component("messenger")
public class MessageThrower 
{
	@Autowired
	private RuntimeService runtimeService;
	
	public void sendMessage(String messageId,int docid)
	{
		for(Execution exec : runtimeService.createExecutionQuery().messageEventSubscriptionName(messageId).list())
		{
			runtimeService.messageEventReceived(messageId, exec.getId(), makeMap(docid));
		}
	}
	private Map<String, Object> makeMap(int docid)
	{
		Map<String, Object> map = new HashMap<String, Object>();
		map.put("docid", docid);
		return map;
	}
}
