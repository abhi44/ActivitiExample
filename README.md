Activiti:-

	Create Two process
	1- First create one process with intermediate message catching event which will wait for the message to move furthur.
	2- second create another process with a service task(as we dont have intermediate message throwing event) which will through the message to a particular process.
	use message reference in the processes to trigger particular message catching event.
	In the expression property of the service task we have to call that service which will help us to throw the message in the expretion property.
	
	
For the service task what we can do?

	Make a custom spring bean that throws a message and allows our workflow to continue after the wait for message(in place for wait for signal).
	We can also send some values through the service task to the message catching event and we can use those value in that process.
	